#!/bin/sh -e

VERSION=$2
TAR=../tomcat9_$VERSION.orig.tar.xz
DIR=tomcat9-$VERSION
TAG=$(echo TOMCAT_$VERSION | sed -e 's/[\.~]/_/g')

svn export https://svn.apache.org/repos/asf/tomcat/tags/$TAG $DIR
tar -c -J -f $TAR --exclude 'taglibs-standard-*.jar' $DIR
rm -rf $DIR ../$TAG $3
